# COURIER SERVER

## WARNING

The Docker images related to the Courier Server are currently private.

The installation is under heavy construction. This currently assumes a Kubernetes cluster on Digital
Ocean (the persistent volume claim is set for a Digital Ocean Volume). It is recommended that this
cluster contains three nodes of about 4GB RAM and 2vCPU machines.

## PREREQUISITES

* A working kubernetes cluster and `kubectl` configuration
* The [Helm](https://helm.sh/) package manager for kubernetes, version 3

## INSTALLATION

The Courier Server is split into separate stand-alone Helm charts.

Each of them have a `values.yaml` file in their main folder, which can be copied to the repository's
root folder and filled. For convenience, they can be combined into one values file.

### OBSERVABILITY

The first chart to install can be found in the `helm-charts/observability/`. This will install what
is needed to log, monitor and trace the Courier Server. Currently, only logging is implemented.

It can be installed by executing the following:

    helm install --generate-name --values ./values.yaml ./helm-chars/observability/

This will install the following:

* [Loki](https://grafana.com/oss/loki), a log aggregator
* Promtail, a log collector
* [Grafana](https://grafana.com/grafana/), a metrics dashboard
* Fluent-Bit and Prometheus, although they could not yet be removed (bug?)

### MAIN

The main Helm chart can be found in the `helm-charts/courier-server` folder.

It can be installed by executing the following:

    helm install --generate-name --values ./values.yaml ./helm-charts/courier-server/

This will install the following:

* three replicas of the `admin-ui` service behind a load balancer on port 80
* three replicas of the `external-api` service behind a load balancer on port 3003
* three replicas of the `delivery-handler` service
* three replicas of the NATS messaging queue service
* six replicas of the `internal-api` service
* one replica of a postgres `database` on a [DigitalOcean volume](https://www.digitalocean.com/docs/volumes/)

## POST-INSTALLATION

After installation, a kubernetes job (`database-setup`) will setup the database automatically. Once
this is done, all pods should soon be running and ready.

The Admin UI's `.env.production` file should point to a host that is easily configurable to
redirect to the External API load balancer. For example, if `VUE_APP_EXTERNAL_API_HOST` is set to
`external-api.example.com`, then the `example.com` DNS could have an A record that points the host
`external-api.example.com` to the External API's load balancer. Something similar can be done for
the Admin UI.

The only thing left is to configure the Courier Server:

* From the Admin UI, create some `Client` objects and set their `uri` attributes to the clients'
routes that should be used to retrieve delivery fields, eg. `http://client-mock.example.com:3099/deliveryFields`
* Create some `MessageKind` objects that will represent _why_ a message is being sent, eg. someone's
invoice is ready
* Create some `DeliveryKind` objects that will represent _how_ a message can be sent, eg. by email.

## USAGE

If everything is setup correctly, the Admin UI can be used to create a new message. The `target
messageable` attribute is arbitrary and should only mean something to the client application.

The Courier Server will then make a request to the client application's URI and ask for delivery
fields. Based on the response, the Courier Server will use the appropriate Courier Adapter to
deliver the message.

## LOCAL DEVELOPMENT

The easiest way to develop and test locally is to use the `docker-compose.yaml` files provided in
each repository. Running `docker-compose up` from a repository will spawn up containers for each
dependencies.

See [docker-compose](https://docs.docker.com/compose/) for more information.

All environment variables can be left to their default values, with the exception of the Delivery
Handler.

If [SendGrid](https://sendgrid.com/) is to be used for email delivery:

* an account with SendGrid

* the `SENDGRID_DEFAULT_FROM_EMAIL_ADDRESS` environment variable set to the default `from` email
address

* the `SENDGRID_API_KEY` environment variable set to the SendGrid API key

If [Twilio](https://twilio.com/) is to be used for text message delivery:

* an account with Twilio

* the `TWILIO_DEFAULT_FROM_PHONE_NUMBER` environment variable set to the default `from` phone number

* the `TWILIO_ACCOUNT_SID` environment variable set to the Twilio Account SID

* the `TWILIO_AUTH_TOKEN` environment variable set to the Twilio Auth Token

## HOW IT WORKS

The typical Courier Server flow is the following:

* the Admin UI or External API receives a request for message delivery
* the message is saved to the database using the Internal API
* the message is published to the messaging queue, where it waits until it is ready to be processed
* the Delivery Handler receives the message from the messaging queue
* the Delivery Handler makes a request to the Client and asks for delivery fields, such as how to
send the message, and what the actual message should be
* the Delivery Handler makes a request, through a Courier Adapter, to a third-party service to
perform the delivery

Example request to the External API listening on port 3003:

* POST /messages

      curl -XPOST http://localhost:3003/messages \
        -H 'Content-Type: application/json' \
        -d '{ "messageKind": "order_created", \
              "targetMessageable": "user_236" }'

  The `messageKind` defines what happened so that a message needed to be sent. It is configured in
  the Courier Server and the related client.

  The `targetMessageable` is a string uniquely representing a user, or anything that could receive
  a communication. The Courier Server has no knowledge about what the string means - it is only
  relevant to the client.

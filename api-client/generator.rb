#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'securerandom'

def generate_postman_v2_1_collection
  postman_data = {
    'info': {
      '_postman_id': uuid,
      'name': 'Courier Bot',
      'schema': 'https://schema.getpostman.com/json/collection/v2.1.0/collection.json'
    },
    'item': []
  }

  data = data('{{internal_api_host}}', '{{external_api_host}}')
  generate_postman_v2_1_nodes(data, postman_data, 0)

  postman_data.to_json
end

def generate_postman_v2_1_environment
  {
    'id': uuid,
    'name': 'development',
    'values': [
      {
        'key': 'internal_api_host',
        'value': 'localhost:3000',
        'type': 'text',
        'description': '',
        'enabled': true
      },
      {
        'key': 'external_api_host',
        'value': 'localhost:3003',
        'type': 'text',
        'description': '',
        'enabled': true
      }
    ],
    '_postman_variable_scope': 'environment',
    '_postman_exported_at': current_time_iso8601_ms,
    '_postman_exported_using': 'courier-bot-api-generator'
  }.to_json
end

def generate_postman_v2_1_nodes(nodes, current_item, depth)
  nodes.each do |node|
    current_item[:item] << generate_postman_v2_1_node(node, current_item, depth)
  end
end

def generate_postman_v2_1_node(node, current_item, depth)
  if node.key?(:children)
    item = {
      'name': node[:name],
      'item': []
    }

    generate_postman_v2_1_nodes(node[:children], item, depth + 1)
    item[:'_postman_isSubFolder'] = true if depth == 0

    item
  else
    url = "#{node[:protocol]}://#{node[:host]}#{node[:path]}"
    node[:path] = node[:path][1..-1] if node[:path].start_with?('/')

    body_content =
      if node[:body]
        node[:body].to_json(space: ' ', indent: '  ', object_nl: "\n")
      else
        ''
      end

    {
      'name': node[:name],
      'request': {
        'method': node[:method],
        'header': node[:headers].map do |k, v|
          {
            'key': k,
            'value': v,
            'type': 'text'
          }
        end,
        'body': {
          'mode': 'raw',
          'raw': body_content
        },
        'url': {
          'raw': url,
          'protocol': node[:protocol],
          'host': [
            node[:host]
          ],
          'path': node[:path].split('/')
        }
      },
      'response': []
    }
  end
end

def generate_insomnia_v4
  insomnia_data = {
    '_type': 'export',
    '__export_format': 4,
    '__export_date': current_time_iso8601_ms,
    '__export_source': 'courier-bot-api-generator',
    'resources': []
  }

  workspace_id = 'wrk_' + uuid_no_dash
  insomnia_data[:resources] << {
    '_id': workspace_id,
    '_type': 'workspace',
    'name': 'Courier Bot',
    'description': '',
    'parentId': nil,
    'created': current_time_usec,
    'modified': current_time_usec
  }

  base_environment_id = 'env_' + uuid_no_dash
  insomnia_data[:resources] << {
    '_id': base_environment_id,
    '_type': 'environment',
    'name': 'Base Environment',
    'color': nil,
    'data': {},
    'dataPropertyOrder': nil,
    'isPrivate': false,
    'parentId': workspace_id,
    'created': current_time_usec,
    'modified': current_time_usec,
    'metaSortKey': 0
  }

  dev_environment_id = 'env_' + uuid_no_dash
  insomnia_data[:resources] << {
    '_id': dev_environment_id,
    '_type': 'environment',
    'name': 'Development',
    'color': nil,
    'data': {
      'internal_api_host': 'localhost:3000',
      'external_api_host': 'localhost:3003'
    },
    'dataPropertyOrder': {
      '&': %w[
        internal_api_host
        external_api_host
      ]
    },
    'isPrivate': false,
    'parentId': base_environment_id,
    'created': current_time_usec,
    'modified': current_time_usec,
    'metaSortKey': 1
  }

  insomnia_data[:resources] << {
    '_id': 'jar_' + uuid_no_dash,
    '_type': 'cookie_jar',
    'name': 'Default Jar',
    'cookies': [],
    'parentId': workspace_id,
    'created': current_time_usec,
    'modified': 0
  }

  data = data('{{internal_api_host}}', '{{external_api_host}}')
  resources = []
  generate_insomnia_v4_nodes(data, workspace_id, resources)
  insomnia_data[:resources] += resources

  insomnia_data.to_json
end

def generate_insomnia_v4_nodes(nodes, parent_id, current_resources)
  nodes.each_with_index do |node, index|
    generate_insomnia_v4_node(node, parent_id, index, current_resources)
  end
end

def generate_insomnia_v4_node(node, parent_id, index, current_resources)
  if node.key?(:children)
    child_id = 'fld_' + uuid_no_dash
    current_resources << {
      '_id': child_id,
      '_type': 'request_group',
      'name': node[:name],
      'description': '',
      'environment': {},
      'environmentPropertyOrder': nil,
      'parentId': parent_id,
      'created': current_time_usec,
      'modified': current_time_usec,
      'metaSortKey': index
    }

    generate_insomnia_v4_nodes(node[:children], child_id, current_resources)
  else
    url = "#{node[:protocol]}://#{node[:host]}#{node[:path]}"

    body =
      if node[:body]
        {
          'mimeType': 'application/json',
          'text': node[:body].to_json(space: ' ', indent: '  ', object_nl: "\n")
        }
      else
        {}
      end

    current_resources << {
      '_id': 'req_' + uuid_no_dash,
      '_type': 'request',
      'name': node[:name],
      'description': '',
      'method': node[:method],
      'url': url,
      'parameters': [],
      'authentication': {},
      'headers': node[:headers].map do |k, v|
        {
          'id': 'pair_' + uuid_no_dash,
          'name': k,
          'value': v
        }
      end,
      'body': body,
      'isPrivate': false,
      'settingDisableRenderRequestBody': false,
      'settingEncodeUrl': true,
      'settingRebuildPath': true,
      'settingSendCookies': true,
      'settingStoreCookies': true,
      'parentId': parent_id,
      'created': current_time_usec,
      'modified': current_time_usec,
      'metaSortKey': index
    }
  end
end

def current_time_iso8601_ms
  @current_time_iso8601 ||= Time.now.utc.strftime('%FT%T.000Z')
end

def current_time_usec
  @current_time_usec ||= Time.now.to_i * 1000
end

def uuid
  SecureRandom.uuid
end

def uuid_no_dash
  uuid.tr('-', '')
end

def data(internal_api_host, external_api_host)
  get_headers = {
    Accept: 'application/json',
    'Accept-Version': 'v1'
  }

  post_headers = {
    Accept: 'application/json',
    'Accept-Version': 'v1',
    'Content-Type': 'application/json'
  }

  [{
    name: 'Courier Client',
    children: []
  }, {
    name: 'Courier Server',
    children: [{
      name: 'External API',
      children: [{
        name: '/adapters',
        children: [{
          name: '/adapters',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters',
          headers: get_headers
        }, {
          name: '/adapters',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters',
          headers: get_headers
        }, {
          name: '/adapters',
          method: 'POST',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters',
          headers: post_headers,
          body: {
            "code": 'adapter_code',
            "uri": 'adapter_name'
          }
        }, {
          name: '/adapters/{code}',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters/adapter_code',
          headers: get_headers
        }, {
          name: '/adapters/{code}',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters/adapter_code',
          headers: get_headers
        }, {
          name: '/adapters/{code}',
          method: 'PATCH',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters/adapter_code',
          headers: post_headers,
          body: {
            "uri": 'adapter_name'
          }
        }, {
          name: '/adapters/{code}',
          method: 'DELETE',
          protocol: 'http',
          host: external_api_host,
          path: '/adapters/adapter_code',
          headers: get_headers
        }]
      }, {
        name: '/deliveryKinds',
        children: [{
          name: '/deliveryKinds',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds',
          headers: get_headers
        }, {
          name: '/deliveryKinds',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds',
          headers: get_headers
        }, {
          name: '/deliveryKinds',
          method: 'POST',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds',
          headers: post_headers,
          body: {
            "code": 'message_kind_code',
            "deliveryMethod": 'delivery_method_code',
            "adapter": 'adapter_code'
          }
        }, {
          name: '/deliveryKinds/{code}',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds/message_kind_code',
          headers: get_headers
        }, {
          name: '/deliveryKinds/{code}',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds/message_kind_code',
          headers: get_headers
        }, {
          name: '/deliveryKinds/{code}',
          method: 'PATCH',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds/message_kind_code',
          headers: post_headers,
          body: {
            "deliveryMethod": 'delivery_method_code',
            "adapter": 'adapter_code'
          }
        }, {
          name: '/deliveryKinds/{code}',
          method: 'DELETE',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryKinds/message_kind_code',
          headers: get_headers
        }]
      }, {
        name: '/deliveryMethods',
        children: [{
          name: '/deliveryMethods',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods',
          headers: get_headers
        }, {
          name: '/deliveryMethods',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods',
          headers: get_headers
        }, {
          name: '/deliveryMethods',
          method: 'POST',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods',
          headers: post_headers,
          body: {
            "code": 'delivery_method_code'
          }
        }, {
          name: '/deliveryMethods/{code}',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods/delivery_method_code',
          headers: get_headers
        }, {
          name: '/deliveryMethods/{code}',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods/delivery_method_code',
          headers: get_headers
        }, {
          name: '/deliveryMethods/{code}',
          method: 'PATCH',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods/delivery_method_code',
          headers: post_headers,
          body: {
          }
        }, {
          name: '/deliveryMethods/{code}',
          method: 'DELETE',
          protocol: 'http',
          host: external_api_host,
          path: '/deliveryMethods/delivery_method_code',
          headers: get_headers
        }]
      }, {
        name: '/messages',
        children: [{
          name: '/messages',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/messages',
          headers: get_headers
        }, {
          name: '/messages',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/messages',
          headers: get_headers
        }, {
          name: '/messages',
          method: 'POST',
          protocol: 'http',
          host: external_api_host,
          path: '/messages',
          headers: post_headers,
          body: {
            "messageKind": 'message_kind_code',
            "targetMessageable": 'target_messageable',
            "status": 'pending'
          }
        }, {
          name: '/messages/{id}',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/messages/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/messages/{id}',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/messages/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/messageKinds',
        children: [{
          name: '/messageKinds',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds',
          headers: get_headers
        }, {
          name: '/messageKinds',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds',
          headers: get_headers
        }, {
          name: '/messageKinds',
          method: 'POST',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds',
          headers: post_headers,
          body: {
            "code": 'message_kind_code',
            "adapter": 'adapter_code'
          }
        }, {
          name: '/messageKinds/{code}',
          method: 'OPTIONS',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds/message_kind_code',
          headers: get_headers
        }, {
          name: '/messageKinds/{code}',
          method: 'GET',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds/message_kind_code',
          headers: get_headers
        }, {
          name: '/messageKinds/{code}',
          method: 'PATCH',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds/message_kind_code',
          headers: post_headers,
          body: {
            "adapter": 'adapter_code'
          }
        }, {
          name: '/messageKinds/{code}',
          method: 'DELETE',
          protocol: 'http',
          host: external_api_host,
          path: '/messageKinds/message_kind_code',
          headers: get_headers
        }]
      }]
    }, {
      name: 'Internal API',
      children: [{
        name: '/adapters',
        children: [{
          name: '/adapters',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/adapters',
          headers: get_headers
        }, {
          name: '/adapters',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/adapters',
          headers: post_headers,
          body: {
            "code": 'adapter_code',
            "uri": 'adapter_name'
          }
        }, {
          name: '/adapters/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/adapters/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/adapters/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/adapters/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "code": 'adapter_code',
            "uri": 'adapter_name'
          }
        }, {
          name: '/adapters/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/adapters/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/deliveries',
        children: [{
          name: '/deliveries',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveries',
          headers: get_headers
        }, {
          name: '/deliveries',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveries',
          headers: post_headers,
          body: {
            "messageId": '00000000-0000-0000-0000-000000000000',
            "deliveryKindId": '00000000-0000-0000-0000-000000000000',
            "externalId": 'external_id',
            "status": 'pending'
          }
        }, {
          name: '/deliveries/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveries/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/deliveries/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveries/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "messageId": '00000000-0000-0000-0000-000000000000',
            "deliveryKindId": '00000000-0000-0000-0000-000000000000',
            "externalId": 'external_id',
            "status": 'pending',
            "undeliveredReason": nil
          }
        }, {
          name: '/deliveries/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveries/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/deliveryFields',
        children: [{
          name: '/deliveryFields',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryFields',
          headers: get_headers
        }, {
          name: '/deliveryFields',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryFields',
          headers: post_headers,
          body: {
            "deliveryId": "00000000-0000-0000-0000-000000000000",
            "name": 'field_name',
            "value": 'field_value'
          }
        }, {
          name: '/deliveryFields/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryFields/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/deliveryFields/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryFields/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "deliveryId": "00000000-0000-0000-0000-000000000000",
            "name": 'field_name',
            "value": 'field_value'
          }
        }, {
          name: '/deliveryFields/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryFields/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/deliveryKinds',
        children: [{
          name: '/deliveryKinds',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryKinds',
          headers: get_headers
        }, {
          name: '/deliveryKinds',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryKinds',
          headers: post_headers,
          body: {
            "code": 'delivery_kind_code',
            "deliveryMethodId": '00000000-0000-0000-0000-000000000000',
            "adapterId": '00000000-0000-0000-0000-000000000000'
          }
        }, {
          name: '/deliveryKinds/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryKinds/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/deliveryKinds/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryKinds/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "code": 'delivery_kind_code',
            "deliveryMethodId": '00000000-0000-0000-0000-000000000000',
            "adapterId": '00000000-0000-0000-0000-000000000000'
          }
        }, {
          name: '/deliveryKinds/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryKinds/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/deliveryMethods',
        children: [{
          name: '/deliveryMethods',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryMethods',
          headers: get_headers
        }, {
          name: '/deliveryMethods',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryMethods',
          headers: post_headers,
          body: {
            "code": 'delivery_method_code'
          }
        }, {
          name: '/deliveryMethods/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryMethods/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/deliveryMethods/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryMethods/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "code": 'delivery_method_code'
          }
        }, {
          name: '/deliveryMethods/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/deliveryMethods/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/messages',
        children: [{
          name: '/messages',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/messages',
          headers: get_headers
        }, {
          name: '/messages',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/messages',
          headers: post_headers,
          body: {
            "messageKindId": '00000000-0000-0000-0000-000000000000',
            "targetMessageable": 'target_messageable',
            "status": 'pending'
          }
        }, {
          name: '/messages/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/messages/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/messages/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/messages/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "messageKindId": '00000000-0000-0000-0000-000000000000',
            "targetMessageable": 'target_messageable',
            "status": 'pending'
          }
        }, {
          name: '/messages/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/messages/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }, {
        name: '/messageKinds',
        children: [{
          name: '/messageKinds',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/messageKinds',
          headers: get_headers
        }, {
          name: '/messageKinds',
          method: 'POST',
          protocol: 'http',
          host: internal_api_host,
          path: '/messageKinds',
          headers: post_headers,
          body: {
            "code": 'message_kind_code',
            "adapterId": '00000000-0000-0000-0000-000000000000'
          }
        }, {
          name: '/messageKinds/{id}',
          method: 'GET',
          protocol: 'http',
          host: internal_api_host,
          path: '/messageKinds/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }, {
          name: '/messageKinds/{id}',
          method: 'PATCH',
          protocol: 'http',
          host: internal_api_host,
          path: '/messageKinds/00000000-0000-0000-0000-000000000000',
          headers: post_headers,
          body: {
            "code": 'message_kind_code',
            "adapterId": '00000000-0000-0000-0000-000000000000'
          }
        }, {
          name: '/messageKinds/{id}',
          method: 'DELETE',
          protocol: 'http',
          host: internal_api_host,
          path: '/messageKinds/00000000-0000-0000-0000-000000000000',
          headers: get_headers
        }]
      }]
    }]
  }]
end

kind = ARGV.shift

case kind
when 'postman'
  target_file = 'postman_v2_1_collection.json'
  output = generate_postman_v2_1_collection
  File.open(target_file, 'w') { |file| file.write(output) }

  target_file = 'postman_v2_1_environment.json'
  output = generate_postman_v2_1_environment
  File.open(target_file, 'w') { |file| file.write(output) }

when 'insomnia'
  target_file = 'insomnia_v4.json'
  output = generate_insomnia_v4
  File.open(target_file, 'w') { |file| file.write(output) }

else
  puts 'Invalid argument - `postman` or `insomnia` expected.'
end
